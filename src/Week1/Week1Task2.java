package Week1;

import javax.swing.JOptionPane;

public class Week1Task2 {
	
	

	public static void main(String[] args) {
		String aString = JOptionPane.showInputDialog (null, "Enter a string");
		
		int numberOfS =  0;
		for(int i = 0; i < aString.length(); i++) {
			if (aString.charAt(i) == 's') {
				numberOfS++;
			}
		}
		
		JOptionPane.showMessageDialog (null, "You entered " + aString + "\n" + "it contains " + numberOfS + " instance(s) of 's' ");
		   // change to say The number of s's in the string is ...
	}
	

}
