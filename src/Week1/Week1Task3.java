package Week1;

import javax.swing.JOptionPane;

public class Week1Task3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String fullName = JOptionPane.showInputDialog (null, "Enter your full name");
		
		String initials;
		String substr = fullName;
		
		initials = "" + fullName.charAt(0);
		
		while ( substr.indexOf(" ") !=  -1) {
			
			substr = substr.substring(substr.indexOf(" ")+1, substr.length());
			initials = initials + substr.charAt(0);
			
		}
	
		JOptionPane.showMessageDialog(null, "Your initials are: " + initials);
	}

}
