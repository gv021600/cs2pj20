package Week1;

import java.util.Iterator;

import javax.swing.JOptionPane;

public class Week1Task4 {
	
	public static int countNamesInString(String string) {
		int names = 0;
				
		String substr = string;
		
		while ( substr.indexOf(" ") !=  -1) {
			
			substr = substr.substring(substr.indexOf(" ")+1, substr.length());
			names++;
			
		}
		return names;
	}
	
	public static String[] populateNamesArray(String allNames, int numberOfNames) {
		String substr = allNames;
		String[] populatedArray = new String[numberOfNames];
		String currentName;
		
		for (int i = 0; i < numberOfNames; i++) {
			if( substr.indexOf(" ") != -1) {
				currentName = substr.substring(0,substr.indexOf(" "));
				substr = substr.substring(substr.indexOf(" ")+1, substr.length());

			} else {
				currentName = substr;
			}
			populatedArray[i] = currentName;
		}
		
		return populatedArray;
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String allNames = JOptionPane.showInputDialog (null, "Enter all names, seperated by space");
		
		String[] arrayOfNames = populateNamesArray(allNames,countNamesInString(allNames));
		
		for (int i = 0; i < arrayOfNames.length; i++) {
			System.out.print(arrayOfNames[i]);
		}


	}

}
