package FullRobotGUI;

public class Obstacle extends ArenaItem {

	
	/**
	 * 
	 */
	public Obstacle() {
		super(100, 100,100);
		col = 'b';
	}
	
	/**
	 * 
	 */
	public Obstacle(double ix, double iy, double ir) {
		super(ix, iy,ir);
		col = 'b';
	}
	
	@Override
	protected String getStrType() {
		
		return "Obstacle";
	}
	

	protected void checkBall(RobotArena arena) {
		
	}

	protected void adjustBall() {
		
	}

}
