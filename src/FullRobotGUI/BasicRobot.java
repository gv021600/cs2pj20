package FullRobotGUI;

public class BasicRobot extends Robot {
	
	
	/** 
	 * Create basic robot, size ir ay ix,iy, moving at angle ia and speed is
	 * color = red
	 * @param ix
	 * @param iy
	 * @param ir
	 * @param ia
	 * @param is
	 */
	public BasicRobot(double ix, double iy, double ir, double ia, double is) {
		super(ix, iy, ir, ia, is);
		col = 'r';
	}
	
	@Override
	public void drawItem(MyCanvas mc) {
		mc.showRobot(x, y, rad, col, rAngle);
	}
	/**
	 *returns robot type 
	 *Basic robot
	 */
	@Override
	protected String getStrType() {
		return "Basic Robot"; 
	}
	/**
	 * checkBall
	 * change angle if hitting wall or another robot
	 */
	@Override
	protected void checkBall(RobotArena arena) {
		rAngle = arena.CheckEntityAngle(x, y, rad, rAngle, this.itemID);

	}

	/**
	 * adjustBall
	 * Here, move ball depending on speed and angle
	 */
	@Override
	protected void adjustBall() {
		double radAngle = rAngle*Math.PI/180;		// put angle in radians
		x += rSpeed * Math.cos(radAngle);		// new X position
		y += rSpeed * Math.sin(radAngle);		// new Y position
	}

}
