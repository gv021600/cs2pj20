package FullRobotGUI;

abstract class Robot extends ArenaItem {
	
	double rAngle, rSpeed;	// angle and speed of travel
	
	
	public Robot() {
		// TODO Auto-generated constructor stub
	}
	
	/** robot abstract constructor,  size ir ay ix,iy, moving at angle ia and speed is
	 * @param ix
	 * @param iy
	 * @param ir
	 * @param ia
	 * @param is
	 */
	public Robot(double ix, double iy, double ir, double ia, double is) {
		super(ix, iy, ir);
		rAngle = ia;
		rSpeed = is;
	}
	
	/**
	 *Abstract for returning Robot type now
	 */
	protected abstract String getStrType();

}
