package FullRobotGUI;


public abstract class ArenaItem {
	protected double x, y, rad;						// position and size of ball
	protected char col;								// used to set colour
	static int itemCounter = 0;
	protected int itemID;							// unique identifier for item

	
	ArenaItem() {
		this(100,100,10);
	}
	
	/**
	 * construct an arena item of radius ir at ix,iy
	 * @param ix
	 * @param iy
	 * @param ir
	 */
	ArenaItem(double ix, double iy, double ir) {
		this.x = ix;
		this.y = iy;
		this.rad = ir;
		this.itemID = itemCounter++;
		this.col = 'w';
	}
	/**
	 * return x position
	 * @return
	 */
	public double getX() { return x; }
	/**
	 * return y position
	 * @return
	 */
	public double getY() { return y; }
	/**
	 * return radius of ball
	 * @return
	 */
	public double getRad() { return rad; }
	/** 
	 * set the ball at position nx,ny
	 * @param nx
	 * @param ny
	 */
	public void setXY(double nx, double ny) {
		x = nx;
		y = ny;
	}
	/**
	 * return itemID
	 * @return
	 */
	public int getID() {return itemID; }
	
	public void drawItem(MyCanvas mc) {
		mc.showCircle(x, y, rad, col);
	}
	/** 
	 * return string describing item
	 */
	public String toString() {
		return getStrType()+" at "+Math.round(x)+", "+Math.round(y);
	}
	
	/**
	 * Return type of object
	 * @return string describing object
	 */
	protected abstract String getStrType();
	
	/**
	 * is ball at ox,oy size or hitting this ball
	 * @param ox
	 * @param oy
	 * @param or
	 * @return true if hitting
	 */
	public boolean hitting(double ox, double oy, double or) {
		return (ox-x)*(ox-x) + (oy-y)*(oy-y) < (or+rad)*(or+rad);
	}		// hitting if dist between ball and ox,oy < ist rad + or
	
	/** is item hitting the other item
	 * 
	 * @param oItem - the other Item
	 * @return true if hitting
	 */
	public boolean hitting (ArenaItem oItem) {
		return hitting(oItem.getX(), oItem.getY(), oItem.getRad());
	}
	/**
	 * abstract method for checking a ball in arena b
	 * @param b
	 */
	protected abstract void checkBall(RobotArena arena);
	/**
	 * abstract method for adjusting a ball (?moving it)
	 */
	protected abstract void adjustBall();
}
