package FullRobotGUI;

import java.awt.print.Printable;
import java.util.ArrayList;
import java.util.Random;

import ballExample.Ball;

public class RobotArena {
	
	double xSize, ySize;
	private ArrayList<ArenaItem> allArenaItems;
	private Random random;
	
	RobotArena() {
		this(500, 400);
	}
	
	/**
	 * construct arena of size xS by yS
	 * @param xS
	 * @param yS
	 */
	RobotArena(double xS, double yS) {
		this.xSize =  xS;
		this.ySize = yS;
		this.random = new Random();
		allArenaItems =  new ArrayList<ArenaItem>();
		
	}
	
	/**
	 * return arena size in x direction
	 * @return
	 */
	public double getxSize() {
		return xSize;
	}
	/**
	 * return arena size in y direction
	 * @return
	 */
	public double getySize() {
		return ySize;
	}
	
	/** draw all items into interface mc
	 * @param mc
	 */
	public void drawArena(MyCanvas mc) {
		for (ArenaItem item : allArenaItems) {
			item.drawItem(mc);// draw all items
		}
	}
	
	public void addObstacle() {
		allArenaItems.add(new Obstacle(this.random.nextDouble() * this.xSize, this.random.nextDouble() * this.ySize, 15));
	}
	
	public void addObstacle(double x, double y) {
		allArenaItems.add(new Obstacle(x, y, 15));
	}
	
	public void addRobot() {
		allArenaItems.add(new BasicRobot(this.random.nextDouble() * this.xSize-15, this.random.nextDouble() * this.ySize-15, 15, this.random.nextDouble()*360, 4));
	}
	
	public void addRobot(double x, double y) {
		allArenaItems.add(new BasicRobot(x, y, 15, this.random.nextDouble()*360, 20));
	}
	
	/**
	 * return list of strings defining each item
	 * @return
	 */
	public ArrayList<String> describeAll() {
		ArrayList<String> ans = new ArrayList<String>();		// set up empty arraylist
		for (ArenaItem item : allArenaItems) 
			ans.add(item.toString());			
		return ans;												// return string list
	}
	/** 
	 * Check angle of ball ... if hitting wall, rebound; if hitting ball, change angle
	 * @param x				ball x position
	 * @param y				y
	 * @param rad			radius
	 * @param ang			current angle
	 * @param notID			identify of ball not to be checked
	 * @return				new angle 
	 */
	public double CheckEntityAngle(double x, double y, double rad, double ang, int notID) {
		double ans = ang;
		if (x < rad || x > xSize - rad) ans = 180 - ans;
			// if ball hit (tried to go through) left or right walls, set mirror angle, being 180-angle
		if (y < rad || y > ySize - rad) ans = - ans;
			// if try to go off top or bottom, set mirror angle
		
		for (ArenaItem item : allArenaItems) {			
			if (item.getID() != notID && item.hitting(x, y, rad)) 
				ans = 180*Math.atan2(y-item.getY(), x-item.getX())/Math.PI;
		}
				
				// check all balls except one with given id
				// if hitting, return angle between the other ball and this one.
		
		return ans;		// return the angle
	}

	public void checkBalls() {
		for (ArenaItem b : allArenaItems) b.checkBall(this);	// check all balls
	}
	/**
	 * adjust all balls .. move any moving ones
	 */
	public void adjustBalls() {
		for (ArenaItem b : allArenaItems) b.adjustBall();
	}

}
