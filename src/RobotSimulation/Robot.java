package RobotSimulation;

public class Robot {
	private int x, y, id;
	private static int robotCount;
	private Direction dir;
	
	public Robot(int x,int y, Direction dir) {
		// TODO Auto-generated constructor stub
		this.id = robotCount++;
		this.x = x;
		this.y = y;
		this.dir = dir;

	}
	
	public String toString() {
		return "Robot " + this.id + " is at " + this.x + ", " +this.y + " moving " + dir.toString();
	}
	public String fileString() {
		return "" + this.id + " " + this.x + " " + this.y + " " +  dir.toString();
	}
	public boolean isHere(int x, int y) {
		if (this.x == x && this.y == y) {
			return true;
		}
		return false;
	}
	public void displayRobot(ConsoleCanvas canvas) {
		canvas.showIt(x, y, 'R');
	}
	
	//Tests if robot can move in current direction, if not changes direction 
	public void tryToMove(RobotArena arena) {
		int newX = 0, newY = 0;
		if (dir == Direction.East) {
			newX = 1;
			newY = 0;
		}
		if (dir == Direction.North) {
			newX = 0;
			newY = -1;
		}
		if (dir == Direction.South) {
			newX = 0;
			newY = 1;
		}
		if (dir == Direction.West) {
			newX = -1;
			newY = 0;
		}
		
		if (arena.canMoveHere(this.x + newX, this.y +newY)) {
			this.x = this.x + newX;
			this.y = this.y +newY;
		} else {
			dir = dir.next();
		}
		
		
	}
	public static void main(String[] args) {
		Robot d = new Robot(5,3, Direction.East);		// create Robot
		System.out.println(d.toString());	// print where is
		Robot e = new Robot(5,3, Direction.South);		// create Robot
		System.out.println(e.toString());	//print second to see if id works
		
	}


}
