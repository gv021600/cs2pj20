package RobotSimulation;

public class ConsoleCanvas {
	private int sizeX, sizeY;
	private char[][] canvas;
	
	public ConsoleCanvas(int sizeX, int sizeY, String studentNumber) {
		// TODO Auto-generated constructor stub
		
		//Canvas size is arena size +2 to account for the border
		this.sizeX = sizeX+2;
		this.sizeY = sizeY+2;
		canvas = new char[this.sizeX][this.sizeY];
		
		// Generate blank canvas with borders
		for (int i = 0; i < this.sizeX; i++) {
			for (int j = 0; j < this.sizeY; j++) {
				if (i == 0 || i == this.sizeX-1 || j == 0 || j == this.sizeY-1) {
					canvas[i][j] = '#';
				} else {
					canvas[i][j] = ' ';
				}
			}
		}
		
		//Include student number in the border if border is long enough
		if (studentNumber.length() > this.sizeX) {
			System.out.println("WARNING: student number doesnt fit in arena border");				
		} else {
			char[] studentNumberArray = studentNumber.toCharArray();
			for (int i = 0; i < studentNumberArray.length; i++) {
				canvas[(sizeX/2)-3+i][0] = studentNumberArray[i];
			}
			
		}
	}
	
	
	public String toString() {
		String canvasString = "";
		for (int i = 0; i < this.sizeY; i++) {
			canvasString = canvasString + "\n";
			for (int j = 0; j < this.sizeX; j++) {
				canvasString = canvasString + this.canvas[j][i];
			}
		}
		canvasString = canvasString + "\n";
		return canvasString;
	}
	
	public void showIt(int posX, int posY, char symbol) {
		this.canvas[posX+1][posY+1] = symbol; //+1 to the position to account for border
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ConsoleCanvas c = new ConsoleCanvas (10, 5, "31021600");	// create a canvas
		c.showIt(4,3,'R');							// add a Robot at 4,3
		System.out.println(c.toString());				// display result

	}

}
