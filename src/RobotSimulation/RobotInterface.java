package RobotSimulation;

import java.util.Scanner;

/**
 * Simple program to show arena with multiple robots
* @author shsmchlr
 *
 */

public class RobotInterface {
	
	private Scanner s;								// scanner used for input from user
    private RobotArena myArena;				// arena in which Robots are shown
    
    
    public void doDisplay() {
    	ConsoleCanvas canvas = new ConsoleCanvas(myArena.getSizeX(), myArena.getSizeY(), "31021600");
    	myArena.ShowRobots(canvas);
    	System.out.print(canvas.toString());
    	
    }
    /**
    	 * constructor for RobotInterface
    	 * sets up scanner used for input and the arena
    	 * then has main loop allowing user to enter commands
     */
    public RobotInterface() {
    	 s = new Scanner(System.in);			// set up scanner for user input
    	 myArena = new RobotArena(20, 6);	// create arena of size 20*6
    	 TextFile tf = new TextFile("Text files", "txt");
    	
        char ch = ' ';
        do {
        	System.out.print("Enter (A)dd Robot,(D)isplay the arena, get (I)nformation, "
        			+ "(M)ove robots, (C)reate new arena, (S)ave arena, (L)oad arena"
        			+ " or e(X)it > ");
        	ch = s.next().charAt(0);
        	s.nextLine();
        	switch (ch) {
    			case 'A' :
    			case 'a' :
        					myArena.addRobot();	// add a new Robot to arena
        					break;
    			case 'C' :
        		case 'c' :
        					System.out.print("Enter size of arena seperated by a space : ");
        					String inputString = s.nextLine();
        					this.myArena = new RobotArena(inputString);
            				break;
        		case 'I' :
        		case 'i' :
        					System.out.print(myArena.toString());
            				break;
        		case 'D' :
        		case 'd' :
        					this.doDisplay();
            				break;
        		case 'M' :
        		case 'm' :
        					myArena.moveAllRobots();
        					this.doDisplay();
        					System.out.print(myArena.toString());
            				break;
        		case 'S' :
        		case 's' :
        					if (tf.createFile()) {
								tf.writeAllFile(this.myArena.fileString());
							}
               				break;
        		case 'L' :
        		case 'l' :
        					if (tf.openFile()) {
								String fileString = tf.readAllFile();
								this.myArena = new RobotArena(fileString.substring(0, fileString.length()-1));
							}
               				break;
        		case 'x' : 	ch = 'X';				// when X detected program ends
        					break;
        	}
    		} while (ch != 'X');						// test if end
        
       s.close();									// close scanner
    }
    
	public static void main(String[] args) {
		RobotInterface r = new RobotInterface();	// just call the interface
	}

}
