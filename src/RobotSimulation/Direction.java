package RobotSimulation;

import java.util.Random;

public enum Direction {
	North,
	East,
	South,
	West;
	
	public Direction next() {
		return values()[(this.ordinal() + 1) % values().length]; 
	}
	
	public static Direction getRandomDirection() {
		Random random = new Random();
		return values()[random.nextInt(values().length)];
	}
	public static Direction fromString(String s) throws Exception {
		for (Direction direction : values()) {
			if (direction.name().equalsIgnoreCase(s)) {
				return direction;
			}
		}
		throw new Exception();
	}
	
}
