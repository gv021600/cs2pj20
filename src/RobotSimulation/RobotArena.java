package RobotSimulation;
import java.util.ArrayList;
import java.util.Random;

public class RobotArena {
	private int sizeX, sizeY;
	private ArrayList<Robot> robots;
	Random randomGenerator;
	
	
	public RobotArena(int x, int y) {
		// Construct arena based on X and Y values
		this.sizeX = x;
		this.sizeY = y;
		this.randomGenerator = new Random();
		this.robots = new ArrayList<Robot>();
		
	}
	public RobotArena(String s) {
		// Construct arena based on pre-existing string
		this.randomGenerator = new Random();
		this.robots = new ArrayList<Robot>();
		
		String[] inputStringSplit =  s.split(";");
		String[] arenaSizes = inputStringSplit[0].split(" ");
		this.sizeX = Integer.parseInt(arenaSizes[0]);
		this.sizeY = Integer.parseInt(arenaSizes[1]);
		
		for (int i = 1; i < inputStringSplit.length; i++) {
			String[] currentRobot = inputStringSplit[i].split(" ");
			
			try {
				this.addRobot(Integer.parseInt(currentRobot[1]), Integer.parseInt(currentRobot[2]), Direction.fromString(currentRobot[3]));
			} catch (Exception e) {
				System.out.print("Error while reading string for robots\n");
			}
		}
	}
	
	public void addRobot() {
		int randX, randY;
		do {
			 randX = randomGenerator.nextInt(sizeX);
			 randY = randomGenerator.nextInt(sizeY);
		} while (getRobotAt(randX, randY) != null);
		this.robots.add(new Robot(randX, randY, Direction.getRandomDirection()));
	}
	
	public void addRobot(int x, int y, Direction dir) {
		this.robots.add(new Robot(x, y, dir));
	}
	public int getSizeX() {
		return sizeX;
	}

	public void setSizeX(int sizeX) {
		this.sizeX = sizeX;
	}

	public int getSizeY() {
		return sizeY;
	}

	public void setSizeY(int sizeY) {
		this.sizeY = sizeY;
	}

	public String toString() {
		String robots_in_text = "";
		for (Robot robot : robots) {
			robots_in_text = robots_in_text + robot.toString() + "\n";
		}
		return robots_in_text;
	}
	public String fileString() {
		String robots_in_text = "";
		String arenaString = "" + this.getSizeX() + " "+ this.getSizeY() + ";";

		for (Robot robot : robots) {
			robots_in_text = robots_in_text + robot.fileString() + ";";
		}
		return arenaString + robots_in_text;
	}
	
	public Robot getRobotAt(int x, int y) {
		
		for (Robot robot : robots) {
			if (robot.isHere(x, y)) {
				return robot;
			}
		}
		
		return null;
	}
	public void ShowRobots(ConsoleCanvas canvas) {
		for (Robot robot : robots) {
			robot.displayRobot(canvas);
		}
	}
	public boolean canMoveHere(int x, int y) {
		if (x > sizeX-1 || x < 0 || y > sizeY-1 || y < 0 || getRobotAt(x, y) != null) {
			return false;
		}
		return true;
	}
	public void moveAllRobots() {
		for (Robot robot : robots) {
			robot.tryToMove(this);
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RobotArena arena = new RobotArena(20, 10);
		arena.addRobot();
		arena.addRobot();
		arena.addRobot();
		System.out.println(arena.toString());

	}

}
