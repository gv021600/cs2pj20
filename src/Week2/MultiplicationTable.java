package Week2;

import java.util.Arrays;

/**
 * class to create a multiplication table
 * @author 
*/
public class MultiplicationTable {

	private int maxNum;				// up to maxNum*maxNum
	private int[][] TableData;			// 2D array to store these
	
	/**
	 * create table for 1*1 up to maxN*maxN
	 * @param maxN
	 */
	MultiplicationTable (int maxN) {	
		// create array of right size, then call makeTable to fill it
		this.maxNum = maxN;
		this.TableData = new int[maxN][maxN];
		this.makeTable();
	}
	/**
	 * function to populate the table
	 */
	private void makeTable() {
		for (int i = 0; i < maxNum; i++) {
			for (int j = 0; j < maxNum; j++) {
				
				this.TableData[i][j]= (i+1)*(j+1);
			}
		}
	}
	/**
	 * return string with the table
	 */
	public String toString() {
		String res = "RJMs Multiplication Table"+"\n";	// title for string
		for (int i = 0; i < maxNum; i++) {
			for (int j = 0; j < maxNum; j++) {
				res = res + "\t" + this.TableData[i][j];

			}
			res = res + "\n";
		}
		
		return res;
	}
	public static void main(String[] args) {
		MultiplicationTable mt = new MultiplicationTable(10);
		System.out.print(mt.toString());
	}

}

