package Week2;

import java.util.Arrays;

import javax.swing.JOptionPane;

public class SmallestDifference {

	private int[] numbers;
	private int smallestDifference;
	private int smallestDifferenceIndex;
	// you may want more private variables
	
	/**
	 * create class : 
	 * @param instr	- string with series of numbers separated by space
	 */
	SmallestDifference (String instr) {
		StringSplitter S = new StringSplitter (instr, " ");
		numbers = S.getIntegers();
	}
	/**
	 * method to search through array of integers and find two pairs of adjacent numbers
	 * which are closest in value; note the difference and where in array
	 */
	public void findSmallest() {
	// you write this
		int currentSmallest = numbers[0] - numbers[1], tempSmallest, currentIndex=0;
		for (int i = 2; i < numbers.length; i++) {
			tempSmallest = numbers[i-1] - numbers[i];
			if (Math.abs(tempSmallest) < Math.abs(currentSmallest)) {
				currentSmallest = tempSmallest;
				currentIndex = i-1;
			}
		}
		this.smallestDifference = Math.abs(currentSmallest);
		this.smallestDifferenceIndex = currentIndex;
    }
	/**
	 * return as string the result of analysis
	 */
	public String toString() {
		return "Smallest difference is " + this.smallestDifference + " at index " + this.smallestDifferenceIndex;		// you extend this
	}
	
	public static void main(String[] args) {
		String userIn = JOptionPane.showInputDialog(null, "Enter series of numbers separated by space > ");
		SmallestDifference sd = new SmallestDifference(userIn);
		sd.findSmallest();
		JOptionPane.showMessageDialog(null, sd.toString());
		
	}

}