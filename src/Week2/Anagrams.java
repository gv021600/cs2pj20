package Week2;

import java.util.HashMap;
import javax.swing.JOptionPane;

public class Anagrams {

	private String[] strings;
	private boolean IsAnagram;
	
	public Anagrams(String bigString) {
		// TODO Auto-generated constructor stub
		StringSplitter s = new StringSplitter(bigString, ";");
		strings = s.getStrings();
	}
	
	public boolean checkAnagram(String string1, String string2) {
		HashMap<Character, Integer> firstMap = new HashMap<Character, Integer>();
		HashMap<Character, Integer> secondMap = new HashMap<Character, Integer>();
		
		//First we treat the strings, trim and lower case
		string1 = string1.trim();
		string1 = string1.toLowerCase();
		string2 = string2.trim();
		string2 = string2.toLowerCase();
		
		//Explode the strings into chars and set it to a hashmap
		
		for (char letter : string1.toCharArray()) {
			firstMap.put(letter, firstMap.getOrDefault(letter, 0)+1);
		}
		
		for (char letter : string1.toCharArray()) {
			secondMap.put(letter, secondMap.getOrDefault(letter, 0)+1);
		}
		
		//compare the two maps
		
		return firstMap.equals(secondMap);
	}
	
	public String toString() {
		
		return "poop";
	}
	public static void main(String[] args) {
//		String userIn = JOptionPane.showInputDialog(null, "Enter two strings separated by ; ");
		String userIn = "The meaning of life;The fine game of nil";
		Anagrams anagrams = new Anagrams(userIn);
		JOptionPane.showMessageDialog(null, anagrams.toString());
		// The meaning of life;The fine game of nil
	}

}
